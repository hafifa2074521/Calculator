window.addEventListener("DOMContentLoaded", (event) => {
  const output = document.getElementById("output");
  const keys = document.getElementsByClassName("key");
  let isOperandPushed = false;
  let exercise = "";
  [...keys].forEach((key) => {
    key.addEventListener("click", (e) => {
      if (key.classList.contains("clear")) {
        output.textContent = 0;
        exercise = "";
      } else if (key.classList.contains("eval")) {
        const result = calc(exerciseToArray(exercise));
        output.textContent = result;
        exercise = result;
      } else if (key.classList.contains("operand")) {
        isOperandPushed = true;
        exercise += key.textContent;
      } else {
        if (isOperandPushed) {
          output.textContent = "";
          isOperandPushed = false;
        }

        output.textContent == 0
          ? (output.textContent = key.textContent)
          : (output.textContent += key.textContent);

        exercise += key.textContent;
      }
    });
  });
});

const operatorPrecedence = [
  { "*": (a, b) => a * b, "/": (a, b) => a / b },
  { "+": (a, b) => a + b, "-": (a, b) => a - b },
];

const calc = (exerciseArray) => {
  let operator;
  for (const operators of operatorPrecedence) {
    const newTokens = [];

    for (const token of exerciseArray) {
      if (token in operators) {
        operator = operators[token];
      } else if (operator) {
        newTokens[newTokens.length - 1] = operator(
          newTokens[newTokens.length - 1],
          token
        );
        operator = null;
      } else {
        newTokens.push(token);
      }
    }

    exerciseArray = newTokens;
  }

  if (exerciseArray.length > 1) {
    alert("error");
    return null;
  } else {
    return exerciseArray[0];
  }
};

const exerciseToArray = (exercise) => {
  const exerciseArray = [];
  const operators = "*/+-";
  let numberStr = "";

  for (const character of exercise) {
    if (operators.includes(character)) {
      if (numberStr === "" && character === "-") {
        numberStr = "-";
      } else {
        exerciseArray.push(parseFloat(numberStr), character);
        numberStr = "";
      }
    } else {
      numberStr += character;
    }
  }

  if (numberStr !== "") {
    exerciseArray.push(parseFloat(numberStr));
  }

  return exerciseArray;
};
